SELECT
	Concat (
		'PARAM',
		CHAR ( 9 ),
		DefinitionGuid,
		CHAR ( 9 ),
		DefinitionName,
		CHAR ( 9 ),
		DefinitionDataType,
		CHAR ( 9 ),-- Need two tab characters here because the shared parameter file schema requires it. There was another value named "DATACATEGORY" at one time.
		CHAR ( 9 ),
		DefinitionGroup,
		CHAR ( 9 ),
		DefinitionVisible,
		CHAR ( 9 ),
		DefinitionDescription,
		CHAR ( 9 ),
		DefinitionUserModifiable 
	) AS Concatenated
FROM
	dbo.ExternalParameterDefinitions 
WHERE
	ExternalParameterDefinitions.IsInSharedParameterFile = 0