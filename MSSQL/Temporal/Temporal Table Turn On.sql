ALTER TABLE dbo.Terminology
ADD PERIOD FOR SYSTEM_TIME ( RowStartUtc, RowEndUtc );

ALTER TABLE dbo.Terminology
SET ( SYSTEM_VERSIONING = ON ( HISTORY_TABLE = dbo.Terminology_History ) );