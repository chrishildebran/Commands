ALTER TABLE dbo.ExternalParameterDefinitions
SET ( SYSTEM_VERSIONING = OFF );

ALTER TABLE dbo.ExternalParameterDefinitions
DROP PERIOD FOR SYSTEM_TIME;