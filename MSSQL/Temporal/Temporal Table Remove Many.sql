SELECT
	p.Name,
	p.temporal_type,
	GrantCmd = 'ALTER TABLE ' + schema_name( p.schema_id ) + '.' + p.name + ' SET (SYSTEM_VERSIONING = OFF);' + CHAR ( 13 ) --+ 'ALTER TABLE ' + schema_name( p.schema_id ) + '.' + p.name + ' DROP PERIOD FOR SYSTEM_TIME;'

FROM
	sys.tables AS p 
WHERE
	p.temporal_type = 2 
ORDER BY
	GrantCmd