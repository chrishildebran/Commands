SELECT
	dbo_Component.Id, dbo_Component.LongDescription, dbo_Component.BusinessModelId
FROM
	dbo_Component
	LEFT JOIN
		dbo_BusinessModel
		ON
			dbo_Component.[BusinessModelId] = dbo_BusinessModel.[Id]
WHERE
	(
		(
			(
				dbo_BusinessModel.Id
			)
			Is Null
		)
	)
;