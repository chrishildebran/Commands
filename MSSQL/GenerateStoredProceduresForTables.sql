DECLARE @TableName sysname = 'Revit' 

SELECT
schema_name( t.schema_id ) AS schema_name,
t.name AS table_name,
CONCAT ( 'USP_',@TableName, '_', t.name, '_CreateOne' ) AS CreateOne,
CONCAT ( 'USP_',@TableName, '_', t.name, '_CreateMany' ) AS CreateMany,
CONCAT ( 'USP_',@TableName, '_', t.name, '_ReadMany' ) AS ReadManySp,
CONCAT ( 'USP_',@TableName, '_', t.name, '_ReadOne' ) AS ReadOne,
CONCAT ( 'USP_',@TableName, '_', t.name, '_UpdateMany' ) AS UpdateMany,
CONCAT ( 'USP_',@TableName, '_', t.name, '_UpdateMany' ) AS UpdateMany,
CONCAT ( 'USP_',@TableName, '_', t.name, '_DeleteMany' ) AS DeleteMany,
CONCAT ( 'USP_',@TableName, '_', t.name, '_DeleteOne' ) AS DeleteOne,
CONCAT ( 'USP_',@TableName, '_', t.name, '_DeleteAll' ) AS DeleteAll,
CONCAT ( t.name, 'DataTransferObject' ) AS DataTransferObjectClassName,
CONCAT ( t.name, 'DataTransferService' ) AS DataTransferServiceClassName 
FROM
	sys.tables t 
WHERE
	t.name NOT LIKE '%History' 
ORDER BY
	schema_name,
	table_name;