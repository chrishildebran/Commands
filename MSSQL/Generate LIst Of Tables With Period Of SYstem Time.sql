SELECT
	p.Name AS TableName,
	p.temporal_type AS TemporalType,
	per.name ,
	per.period_type,
	per.period_type_desc,
	per.start_column_id,
	per.end_column_id,
	GrantCmd = 'ALTER TABLE ' + schema_name( p.schema_id ) + '.' + p.name + ' DROP PERIOD FOR SYSTEM_TIME;' 
FROM
	sys.periods AS per
	INNER JOIN sys.tables p ON p.object_id = per.object_id 
ORDER BY
	GrantCmd -- https://learn.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-periods-transact-sql?view=sql-server-ver16