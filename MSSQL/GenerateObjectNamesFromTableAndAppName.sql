DECLARE @AppName sysname = 'Revit' 

-- https://www.craft.do/s/OcXkO4oAtPmefz

SELECT
	schema_name( t.schema_id ) AS schema_name,
	t.NAME AS table_name,
	
	 
	
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_CreateOne' ) AS CreateOne,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_ReadOne' ) AS ReadOne,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_ReadAll' ) AS ReadAll,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_ReadSome' ) AS ReadSome,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_UpdateAll' ) AS UpdateAll,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_UpdateOne' ) AS UpdateOne,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_UpdateSome' ) AS UpdateSome,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_DeleteAll' ) AS DeleteAll,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_DeleteOne' ) AS DeleteOne,
	CONCAT ( 'USP_', @AppName, '_', t.NAME, '_DeleteSome' ) AS DeleteSome,
	CONCAT ( t.NAME, 'DataTransferObject' ) AS DataTransferObjectName,
	CONCAT ( t.NAME, 'DataTransferService' ) AS DataTransferServiceClassName 
FROM
	sys.TABLES t 
WHERE
	t.NAME NOT LIKE '%History' 
ORDER BY
	schema_name,
	table_name;