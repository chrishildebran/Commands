SELECT
	sys.tables.name AS TableName,
	sys.columns.name AS ColumnName,
	sys.columns.max_length,
	sys.columns.precision,
	sys.columns.scale,
	sys.columns.user_type_id 
FROM
	sys.columns
	INNER JOIN sys.tables ON sys.columns.object_id = sys.tables.object_id --WHERE 	sys.columns.name LIKE '%Name%' --AND user_type_id <> 231
	
WHERE
	sys.columns.precision <> 0 
	AND sys.columns.scale <> 0 
	AND user_type_id <> 42 
ORDER BY
	sys.tables.name,
	sys.columns.name