SELECT
	schema_name( t.schema_id ) AS schema_name,
	t.NAME AS table_name 
FROM
	sys.TABLES t 
WHERE
	t.NAME NOT LIKE '%History' 
ORDER BY
	schema_name,
	table_name