
ALTER TABLE [dbo].[Component] 

ADD CONSTRAINT [FK_FabricationPatternOptionType_Id] 

FOREIGN KEY ( [FabricationPatternOptionsTypeId] ) 

REFERENCES [dbo].[FabricationPatternOptionType] ( [Id] ) 

ON DELETE NO ACTION ON UPDATE NO ACTION