SELECT
	s.name AS SchemaName,
	p.Name,
	has_perms_by_name( p.Name, 'OBJECT', 'EXECUTE' ) AS has_execute,
	GrantCmd = 'GRANT EXECUTE ON OBJECT::[' + s.name + '].[' + + p.name + '] TO [JHKELLY\Detailing]' 
FROM
	sys.procedures AS p
	INNER JOIN sys.schemas s ON p.schema_id = s.schema_id 
--WHERE
--	p.Name LIKE 'VCS%' 
--	OR p.Name LIKE 'USP%' 
ORDER BY
	has_execute,
	GrantCmd