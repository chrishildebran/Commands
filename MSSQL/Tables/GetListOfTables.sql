DECLARE @TableName sysname = 'Revit'
SELECT
    schema_name(t.schema_id) AS schema_name,
    t.name                   AS table_name
FROM
    sys.tables t
		
ORDER BY
    schema_name,
    table_name;