-- ----------------------------
-- Add Fields
-- ----------------------------
ALTER TABLE [workflow].[ApplicationCategory] ADD [UpdatedUtc] datetime2(7) DEFAULT GETUTCDATE() NOT NULL
ALTER TABLE [workflow].[ApplicationCategory] ADD [UpdatedBy] nvarchar(50) DEFAULT SYSTEM_USER NOT NULL

GO

-- ----------------------------
-- Create Triggers
-- ----------------------------
CREATE TRIGGER [workflow].[Tr_ApplicationCategory_Updated] ON [workflow].[ApplicationCategory]
	WITH EXECUTE AS CALLER
FOR

INSERT,

UPDATE AS

UPDATE [workflow].[ApplicationCategory]

SET UpdatedBy = SYSTEM_USER, UpdatedUtc = GETUTCDATE()
FROM inserted
WHERE inserted.id = [workflow].[ApplicationCategory].id

GO