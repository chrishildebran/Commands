-- USAGE
-- Find and Replace "TableName"  
-- Find and Replace "SchemaName"

 

-- ----------------------------
-- Table structure for Schedule
-- ----------------------------
IF EXISTS
    (
        SELECT
            *
        FROM
            sys.all_objects
        WHERE
            object_id = OBJECT_ID(N'[SchemaName].[TableName]')
            AND type IN (
                            'U'
                        )
    )
    DROP TABLE [SchemaName].[TableName]
GO

CREATE TABLE [SchemaName].[TableName]
    (
        [Id]          INT           IDENTITY(1, 1) NOT NULL,
        [Name]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Description] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
            DEFAULT '-' NOT NULL,
        [Notes]       NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
            DEFAULT '-' NOT NULL,
        [UpdatedUtc]  DATETIME2(7)
            DEFAULT GETUTCDATE() NOT NULL,
        [UpdatedBy]   NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS
            DEFAULT '-' NOT NULL
    )
GO

ALTER TABLE [SchemaName].[TableName] SET (LOCK_ESCALATION = TABLE)
GO

-- ----------------------------
-- Auto increment value for Schedule
-- ----------------------------
DBCC CHECKIDENT('[SchemaName].[TableName]', RESEED, 1)
GO

-- ----------------------------
-- Triggers structure for table Schedule
-- ----------------------------
CREATE TRIGGER [SchemaName].[Tr_TableName_Updated]
ON [SchemaName].[TableName]
WITH
    EXECUTE
AS
CALLER
FOR INSERT, UPDATE AS UPDATE
                          [SchemaName].[TableName]
                      SET
                          UpdatedBy = SYSTEM_USER,
                          UpdatedUtc = GETUTCDATE()
                      FROM
                          inserted
                      WHERE
                          inserted.id = [SchemaName].[TableName].[id]
GO

-- ----------------------------
-- Primary Key structure for table Schedule
-- ----------------------------
ALTER TABLE [SchemaName].[TableName]
ADD CONSTRAINT [PK_TableName_Id]
    PRIMARY KEY CLUSTERED ([Id])
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
          ALLOW_PAGE_LOCKS = ON
         ) ON [PRIMARY]
GO
