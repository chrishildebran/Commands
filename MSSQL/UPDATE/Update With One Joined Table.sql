UPDATE baseTable 

SET baseTable.UpdaterId = joinedTable.UpdaterId 

FROM
	dbo.ProjectUpdatersLog baseTable
	
	INNER JOIN [Estimating].[Data] joinedTable ON baseTable.Name = joinedTable.Name