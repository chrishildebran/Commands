-- bt = Base Table
-- st = Second Table
-- tt = Third Table

UPDATE			bt 

SET					bt.[field] = d.[field] 

-- First Table
FROM				[Schema].[BaseTable] bt

-- Second Table
INNER JOIN	[Schema].[SecondTable] st ON bt.[field] = st.[field]
	
-- Third Table
INNER JOIN	[Schema].[ThirdTable] tt ON st.[field] = tt.[field] 
	 
 