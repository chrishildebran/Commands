DECLARE @ToReplace NVARCHAR(100)
DECLARE @Replacement NVARCHAR(100)

SET @ToReplace = 'Poop'
SET @Replacement = 'ExternalCommandData'

UPDATE [Development].[FodyMethodTimer]
SET CalledMethod = REPLACE(CalledMethod, @ToReplace, @Replacement);
