UPDATE dbo.RevitTransactionsLog 
SET dbo.RevitTransactionsLog.Notes = LEFT ( [Notes], CHARINDEX( '|', [Notes], 0 ) - 3 ) 
WHERE
	dbo.RevitTransactionsLog.Notes LIKE '%  |  %' 
	AND dbo.RevitTransactionsLog.TransactionName = 'Arrange Elements'