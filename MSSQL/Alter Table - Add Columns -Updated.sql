ALTER TABLE [revit].[RibbonPanelConfigurationLinkToUserRole] ADD [UpdatedBy] nvarchar(30) NULL
GO
CREATE TRIGGER [revit].[Tr_RibbonPanelConfigurationLinkToUserRole_UpdatedBy] ON [revit].[RibbonPanelConfigurationLinkToUserRole]
FOR UPDATE AS
BEGIN
        UPDATE
                ProductRange
        SET
                UpdatedBy = USER_NAME()
END