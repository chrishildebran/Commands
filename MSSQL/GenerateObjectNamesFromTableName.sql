DECLARE @TableName sysname = 'ButtonPart'
IF
	OBJECT_ID( @TableName, 'U' ) IS NOT NULL 



SELECT
	-- https://www.craft.do/s/OcXkO4oAtPmefz
	CONCAT ( 'USP_', @TableName, '_CreateOne' ) AS CreateOne,
	CONCAT ( 'USP_', @TableName, '_ReadOne' ) AS ReadOne,
	CONCAT ( 'USP_', @TableName, '_ReadAll' ) AS ReadAll,
	CONCAT ( 'USP_', @TableName, '_ReadSome' ) AS ReadSome,
	CONCAT ( 'USP_', @TableName, '_UpdateAll' ) AS UpdateAll,
	CONCAT ( 'USP_', @TableName, '_UpdateOne' ) AS UpdateOne,
	CONCAT ( 'USP_', @TableName, '_UpdateSome' ) AS UpdateSome,
	CONCAT ( 'USP_', @TableName, '_DeleteAll' ) AS DeleteAll,
	CONCAT ( 'USP_', @TableName, '_DeleteOne' ) AS DeleteOne,
	CONCAT ( 'USP_', @TableName, '_DeleteSome' ) AS DeleteSome,
	CONCAT ( @TableName, 'DataTransferObject' ) AS DataTransferObjectName,
	CONCAT ( @TableName, 'DataTransferService' ) AS DataTransferServiceClassName 
	
ELSE 
	
BEGIN
	PRINT 'Table does not exist.' 
END