SET NOCOUNT ON
IF
	TRIGGER_NESTLEVEL ( ) > 1 RETURN 
	
	UPDATE 
	dbo.RevitTransactionsLog 
	
	SET 
	DateUpdated = GETDATE( ),
	UpdatedBy = SUSER_NAME( ) 
	
WHERE
	Id IN ( SELECT DISTINCT Id FROM Inserted )